<?php
include("koneksi/koneksi.php");
?>

<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>Tazkia Tour and Travel</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-primary">
			<div class="container">
				<a class="navbar-brand" href="index.html">
					<img src="images/logo11.png" alt height="50" width="170" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a class="nav-link">Tazkia Tour AND Travel</a></li>	
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Pages</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" >Reservation</a>
								<a class="dropdown-item" >Stuff</a>
								<a class="dropdown-item" >Gallery</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Blog</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" >blog</a>
								<a class="dropdown-item" >blog Single</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="admin">Admin</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
	
	<!-- Start slides -->
	<div id="slides" class="cover-slides">
		<ul class="slides-container">
			<li class="text-left">
				<img src="images/travel.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>Welcome To <br> Tazkia Tour And Travel</strong></h1>
							<p class="m-b-40">International Tour and travel services
								Book from now and get specials cashback until 500rb
								Start from 150rb/person.<br> Our team of experts provides a first class, customer-focused service that won’t let you down. We understand that time is money, so trust us to remove the burden of business travel.</p></p>
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="#">Registrasi</a></p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-left">
				<img src="images/travel3.jfif" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>Welcome To <br> Tazkia Tour And Travel</strong></h1>
							<p class="m-b-40">International Tour and travel services
								Book from now and get specials cashback until 500rb
								Start from 150rb/person. <br> Our team of experts provides a first class, customer-focused service that won’t let you down. We understand that time is money, so trust us to remove the burden of business travel.</p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-left">
				<img src="images/travel2.jfif" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>Welcome To <br>Tazkia Tour And Travel</strong></h1>
							<p class="m-b-40">International Tour and travel services
								Book from now and get specials cashback until 500rb
								Start from 150rb/person. <br> Our team of experts provides a first class, customer-focused service that won’t let you down. We understand that time is money, so trust us to remove the burden of business travel.</p>
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="#">Register</a></p>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<div class="slides-navigation">
			<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
		</div>
	</div>
	<!-- End slides -->
	
	<!-- Start Menu -->
	<div class="menu-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>TAZKIA TOUR AND TRAVEL</h2>
						<p>Perusahaan yang berjalanan dibidang tour and travel yang sudah berdiri sejak 2013 yang berkantor di Kediri</p>
					</div>
				</div>
			</div>
			
			<div class="row inner-menu-box">
				<div class="col-3">
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Paket Travel</a>
						<a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">List Data Administrasi</a>
						<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Peserta</a>
					</div>
				</div>
				
				<div class="col-9">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
						<div class="row">
							<table class="table table-striped ">
									<thead class="table-primary">                  
										<tr>
										<th width="5%"><center>No</center></th>
										<th width="50%"><center>Paket travel</center></th>
										</tr>
									</thead>

									<tbody>
									
										<?php
										//menampilkan data travel
										$sql_dh = "SELECT `kode_travel`, `travel` FROM `travel` ";
										if (isset($_POST["katakunci"])){
										$katakunci_travel = $_POST["katakunci"];
										$sql_dh .= " where `travel` LIKE '%$katakunci_travel%'";
										} 
											$query_dh = mysqli_query($koneksi,$sql_dh);
											$no=+1;
											while($data_dh = mysqli_fetch_row($query_dh)){
											$kode_travel = $data_dh[0];
											$travel = $data_dh[1];

										?>	
											<tr>
											<td><center><?php echo $no;?></center></td>
											<td><?php echo $travel;?></td>
											</tr>
											<?php
											$no++;
										}?>
										</tbody>
									</table>
							</div>	
						</div>

						<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
							<div class="row">
							<table class="table table-striped ">
									<thead class="table-primary">                    
									<tr>
									<th width="5%"><center>No</th>
									<th width="80%"><center>Data Administrasi yang harus dilengkapi</center></th>
									</tr>
								</thead>
								<tbody>
									<?php
										//menampilkan data adm
										$sql_h = "SELECT `kode_adm`, `adm` FROM `adm`";
											if(isset($_POST["katakunci"])){
											$katakunci_adm = $_POST["katakunci"];
											$_SESSION['katakunci_adm'] = $katakunci_adm;
											$sql_h .= " where `adm` LIKE '%$katakunci_adm%'";
											} 
										
										$query_h = mysqli_query($koneksi,$sql_h);
										$no=+1;
										while($data_h = mysqli_fetch_row($query_h)){
										$kode_adm = $data_h[0];
										$adm = $data_h[1];
									?>	
										<tr>
											<td><center><?php echo $no;?></center></td>
											<td><?php echo $adm;?></td>
										</tr>
										<?php
										$no++;
									}?>
								</tbody>
								</table>
							</div>
						</div>	
						<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
							<div class="row">
							<table class="table table-striped ">
									<thead class="table-primary">                  
								<tr>
									<th width="5%"><center>No</center></th>
									<th width="40%"><center>Nama</center></th>
									<th width="20%"><center>Paket Travel</center></th>
								</tr>
								</thead>
								<tbody>
								<?php  
									//menampilkan data peserta
									$sql_mhs = "select `m`.`kode_peserta`, `m`.`nama`, `j`.`travel` 
											from `peserta` `m`
											inner join `travel` `j` 
											on `m`.`kode_travel` = `j`.`kode_travel` "; 
									if (isset($_POST["katakunci"])){
										$katakunci_mhs = $_POST["katakunci"];
										$sql_mhs .= " where `kode_peserta` LIKE '%$katakunci_mhs%' 
										OR `nama` LIKE '%$katakunci_mhs%'";
									} 

									$query_mhs = mysqli_query($koneksi,$sql_mhs);
									$no=+1;
									while($data_mhs = mysqli_fetch_row($query_mhs)){
									$nama = $data_mhs[1];
									$travel = $data_mhs[2];
									?>
									<tr>
										<td><?php echo $no;?></td>
										<td><?php echo $nama;?></td>
										<td><?php echo $travel;?></td>
									</tr>
									<?php $no++; }?>
								</tbody>
							</table>  
						</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Menu -->
	
	<!-- Start Customer Reviews -->
	<div class="customer-reviews-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Customer Reviews</h2>
						<p></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 mr-auto ml-auto text-center">
					<div id="reviews" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner mt-4">
							<div class="carousel-item text-center active">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/quotations-button.png" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Paul Mitchel</strong></h5>
								<h6 class="text-dark m-0">Bali-Jawa</h6>
								<p class="m-0 pt-3">Sangat Murah dan tepat waktu. Pemilihan hotel selalu dekat tempat wisata, jadi tidak perlu jalan jauh untuk ke tempat wisata</p>
							</div>
							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/quotations-button.png" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Steve Fonsi</strong></h5>
								<h6 class="text-dark m-0">Dubai</h6>
								<p class="m-0 pt-3">Murah, hotel berbintang dan maskapai menggunakkan garuda, Pemandu juga sangat aktif bertanya kepada customer untuk memberikan service yang sangat spektakuler, dan makanannya sangat bervariasi</p>
							</div>
							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/quotations-button.png" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Daniel vebar</strong></h5>
								<h6 class="text-dark m-0">Turkie</h6>
								<p class="m-0 pt-3">Murah dan spektakuler</p>
							</div>
						</div>
						<a class="carousel-control-prev" href="#reviews" role="button" data-slide="prev">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#reviews" role="button" data-slide="next">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
							<span class="sr-only">Next</span>
						</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Customer Reviews -->
	
	<!-- Start Contact info -->
<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4 arrow-right">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Phone</h4>
						<p class="lead">
							+62 88 888 8888
						</p>
					</div>
				</div>
				<div class="col-md-4 arrow-right">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							nurevatuln@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Location</h4>
						<p class="lead">
							Malang
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>About Us</h3>
					<p>International Tour and travel services
								Book from now and get specials cashback until 500rb
								Start from 150rb/person. Our team of experts provides a first class, customer-focused service that won’t let you down. We understand that time is money, so trust us to remove the burden of business travel.</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Subscribe</h3>
					<div class="subscribe_form">
						<form class="subscribe_form">
							<input name="EMAIL" id="subs-email" class="form_input" placeholder="Email Address..." type="email">
							<button type="submit" class="submit">SUBSCRIBE</button>
							<div class="clearfix"></div>
						</form>
					</div>
					
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Contact information</h3>
					<p class="lead">Kediri, Jawa Timur</p>
					<p class="lead"><a href="#">+62 888888 88</a></p>
					<p><a href="#"> info@admin.com</a></p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Opening hours</h3>
					<p><span class="text-color">Monday: </span>Closed</p>
					<p><span class="text-color">Tue-Wed :</span> 9:Am - 10PM</p>
					<p><span class="text-color">Thu-Fri :</span> 9:Am - 10PM</p>
					<p><span class="text-color">Sat-Sun :</span> 5:PM - 10PM</p>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>