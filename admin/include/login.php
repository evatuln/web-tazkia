<body class="hold-transition login-page">
	<div class="login-box btn btn-primary" style="background: #1f6f8b; border-color: #03506f; border-radius: 15px;">
	  <div class="login-logo">
	    <a href="index.php" style="color: white; font-family: sans-serif; font-size: 30px;"><b>Admin</b>TAZKIA TRAVEL</a>
	  </div>
	  <!-- /.login-logo -->
	  <div class="card" style="border-radius: 12px;">
	    <div class="card-body login-card-body" style="border-radius: 12px; >
	      <p class="login-box-msg ">Sign in to start your session</p>
        <?php if(!empty($_GET['gagal'])){?>
  	      <?php if($_GET['gagal']=="userKosong"){?>
	          <span class="text-danger">Maaf Username Tidak Boleh Kosong</span>
	        <?php } else if($_GET['gagal']=="passKosong"){?>
	          <span class="text-danger">Maaf Password Tidak Boleh Kosong</span>
	        <?php } else {?>
	          <span class="text-danger">Maaf Username dan Password Anda Salah</span>
	        <?php }?>
	    <?php }?>   
      <form action="index.php?include=konfirmasi_login" method="post">
	        <div class="input-group mb-3">
	          <input type="text" class="form-control"
	            placeholder="Username" name="username">
	          <div class="input-group-append">
	            <div class="input-group-text">
	              <span class="fas fa-user"></span>
	            </div>
	          </div>
	        </div>
          <div class="input-group mb-3">
	          <input type="password" class="form-control" 
                 placeholder="Password" name="password">
          <div class="input-group-append">
	            <div class="input-group-text">
	              <span class="fas fa-lock"></span>
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-8">
	          </div>
	          <!-- /.col -->
	          <div class="col-4">
	            <button type="submit" name="login" value="Login"
	            class="btn btn-success btn-block" style="background: #456268; border-color: #456268;">Sign In</button>
	          </div>
	          <!-- /.col -->
	        </div>
	      </form>
	      <!-- /.social-auth-links -->
	    </div>
	    <!-- /.login-card-body -->
	  </div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery -->
	<?php include("includes/script.php") ?>
	 
	</body>